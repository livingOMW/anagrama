# anagrama

Task description:

The task is to write a program which finds all anagrams of a given word in a
dictionary (wordlist.txt are in src/main/resources)(the anagram must also be part of the dictionary).
The dictionary is a simple text file which contains one word per line. The program
should handle anagram-requests as fast as possible. Consider a pre-processing
of the dictionary to optimize the requests.
