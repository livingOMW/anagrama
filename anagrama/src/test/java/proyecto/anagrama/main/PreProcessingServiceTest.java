package proyecto.anagrama.main;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Objects;

import org.junit.Test;

import proyecto.anagrama.service.PreProcessingService;

public class PreProcessingServiceTest {

	@Test
	public void testPalabrasExistenFichero() throws IOException {
		String [] palabrasExisten = {"MEJOR","oleg","pepe"};
		for (String palabra : palabrasExisten) {
			assertTrue(!Objects.isNull(PreProcessingService.leerOrdenarFichero(palabra)));
		}
	}

	@Test
	public void testPalabrasNoExistenFichero() throws IOException {
		String [] palabrasNoExisten = {"sfshshshshshs345345fsb","sfdghsfdhsdhsh","sfgsfdgsfdgshshsh"};
		for (String palabra : palabrasNoExisten) {
			assertTrue(!Objects.isNull(PreProcessingService.leerOrdenarFichero(palabra)));
		}
	}
}
