package proyecto.anagrama.service;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import proyecto.anagrama.model.Palabra;

public class ResultServiceTest {

	@Test
	public void testBuscarAnagramas() {
		List<Palabra> palabras = new ArrayList<>();
		char[] charParamEntrada ={'d','e','o','p','r'};
		palabras.add(new Palabra("pedro"));
		palabras.add(new Palabra("ordep"));
		palabras.add(new Palabra("pedor"));
		palabras.add(new Palabra("ORped"));
		assertFalse(ResultService.buscarAnagramas(palabras, charParamEntrada).isEmpty());
	}

}
