package proyecto.anagrama.utils;

public class Constants {
	
	public static final String RUTA_FICHERO = "src\\main\\resources\\wordlist.txt";
	public static final String ERROR_FICHERO_NO_ECONTRADO = "ERROR FICHERO NO ECONTRADO.";
	public static final String ERROR_DEL_SISTEMA = "ERROR DEL SISTEMA.";
	public static final String ERROR_PALABRA_NO_EXISTE= "PALABRA: %s NO EXISTE EN EL DICCIONARIO.";
	public static final String ERROR_ENAGRAMA_NO_EXISTE= "ANAGRAMAS NO ENCONTRADAS EN EL DICCIONARIO PARA PALABRA: %s .";
	public static final String ERROR_FALTA_ARGUMENTO = "ERROR DEBE INSERTAR UNA PALABRA COMO ARGUMENTO.";
	public static final String TOTAL_RESULTADO_ECONTRADOS = "TOTAL RESULTADOS ECONTRADOS: %d.";
	public static final String SALIR = "-1";

	
}
