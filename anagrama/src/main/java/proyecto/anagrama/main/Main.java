package proyecto.anagrama.main;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import proyecto.anagrama.model.Palabra;
import proyecto.anagrama.service.PreProcessingService;
import proyecto.anagrama.service.ResultService;
import proyecto.anagrama.utils.Constants;

public class Main 
{

	public static void main(String[] args) throws IOException
    {
		List<Palabra> palabras=PreProcessingService.cargarFichero();
        System.out.println("Inserte una palabra para buscarla en el diccionario o pulse -1 pasa salir:");
        try(Scanner scan = new Scanner (System.in)){
	        while(scan.hasNext()) {
	        	String paramEntrada = scan.next();
	        	if(Constants.SALIR.equals(paramEntrada)) {
	        		break;
	        	}
		    	long startTime = System.currentTimeMillis();
				Palabra palabraBuscar = new Palabra(paramEntrada);
				List<String> anagramExists = ResultService.buscarAnagramas(palabras,palabraBuscar.getCaracteres());
				System.out.println(ResultService.mostrarResultados(paramEntrada, anagramExists));
				long estimatedTime = System.currentTimeMillis() - startTime;
				System.out.println("Tiempo de búsqueda en milesegundos:"  + estimatedTime);
	        }
        }
        System.out.println("Programa ha finalizado.");
    }

}
