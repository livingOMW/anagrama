package proyecto.anagrama.model;

import java.util.Arrays;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Palabra {
	
	private String contenido;
	private char[] caracteres;
	
	public Palabra(String contenido) {
		super();
		this.contenido = contenido;
		this.caracteres = contenido.toLowerCase().toCharArray();
		Arrays.sort(this.caracteres);
	}
}
