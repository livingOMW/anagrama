package proyecto.anagrama.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import proyecto.anagrama.model.Palabra;
import proyecto.anagrama.utils.Constants;

public class PreProcessingService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PreProcessingService.class);

	public static List<Palabra> leerOrdenarFichero(String paramEntrada) throws IOException {
		
	    List<Palabra> palabras =null;
		Path path = Paths.get(Constants.RUTA_FICHERO);
		try(Stream<String> lines = Files.lines(path)){
			LOGGER.debug("<<<<<<<<<<<<<<<<<<<<INICIO PROCESANDO EL FICHERO>>>>>>>>>>>>>>>>>>>");
			palabras = lines.filter(p->p.length()==paramEntrada.length())
					.map(p->new Palabra(p))
					.collect(Collectors.toList());
			LOGGER.debug("<<<<<<<<<<<<<<<<<<<<FIN PROCESANDO EL FICHERO>>>>>>>>>>>>>>>>>>>>>>");
		} catch (NoSuchFileException e) {
			LOGGER.error(Constants.ERROR_FICHERO_NO_ECONTRADO,e);
		}catch(Exception e) {
			LOGGER.error(Constants.ERROR_DEL_SISTEMA,e);
		}
		return palabras;
	}
	
	public static List<Palabra> cargarFichero() throws IOException {
		
	    List<Palabra> palabras =null;
		Path path = Paths.get(Constants.RUTA_FICHERO);
		try(Stream<String> lines = Files.lines(path)){
			LOGGER.debug("<<<<<<<<<<<<<<<<<<<<CARGANDO EL FICHERO EN EL SISTEMA>>>>>>>>>>>>>>>>>>>");
			palabras = lines.map(p->new Palabra(p))
					.collect(Collectors.toList());
			LOGGER.debug("<<<<<<<<<<<<<<<<<<<<FIN CARGANDO EL FICHERO EN EL SISTEMA>>>>>>>>>>>>>>>>>>>>>>");
		} catch (NoSuchFileException e) {
			LOGGER.error(Constants.ERROR_FICHERO_NO_ECONTRADO,e);
		}catch(Exception e) {
			LOGGER.error(Constants.ERROR_DEL_SISTEMA,e);
		}
		return palabras;
	}
}
