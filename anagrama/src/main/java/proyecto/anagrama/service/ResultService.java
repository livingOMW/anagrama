package proyecto.anagrama.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import proyecto.anagrama.model.Palabra;
import proyecto.anagrama.utils.Constants;

public class ResultService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResultService.class);
	
	public static List<String> buscarAnagramas(List<Palabra> palabras,char[] charParamEntrada) {
		List<String> anagramExists = new ArrayList<>(); 
		palabras.forEach(p -> {
		    if(Arrays.equals(p.getCaracteres(), charParamEntrada)) {
		    	anagramExists.add(p.getContenido());
		    }
		});
		return anagramExists;
	}
	
	public static String mostrarResultados(String paramEntrada, List<String> anagramExists) {
		
		StringBuilder resultado = new StringBuilder("RESULTADOS DE LA BÚSQUEDA DE ANAGRAMA EN EL DICCIONARIO:"+System.lineSeparator());
		
		if(!anagramExists.stream().anyMatch(paramEntrada::equalsIgnoreCase)) {
			LOGGER.debug(String.format(Constants.ERROR_PALABRA_NO_EXISTE, paramEntrada));
			resultado.append(String.format(Constants.ERROR_PALABRA_NO_EXISTE, paramEntrada));
		}else {
			if(!anagramExists.isEmpty()) {
				resultado.append(String.format(Constants.TOTAL_RESULTADO_ECONTRADOS,anagramExists.size())+System.lineSeparator());
				anagramExists.forEach(anagrama->{
					resultado.append(anagrama+System.lineSeparator());
				});
			}else {
				resultado.append(String.format(Constants.ERROR_ENAGRAMA_NO_EXISTE, paramEntrada));
			}
		}
		return resultado.toString();
	}
}
